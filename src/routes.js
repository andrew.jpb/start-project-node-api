import { Router } from 'express';

const app = new Router();

app.get('/', (req, res) => res.json({ server: true }));

export default app;
