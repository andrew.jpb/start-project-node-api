nodemon express sucrase prettier eslint eslint-config-prettier eslint-plugin-prettier cors


settings json vs code


{
    "workbench.iconTheme": "material-icon-theme",
    "editor.fontSize": 18,
    "editor.fontFamily": "'Fira Code'",
    "editor.fontLigatures": true,
    "javascript.updateImportsOnFileMove.enabled": "never",
    "explorer.confirmDelete": false,
    

  //esLint Conf
  "eslint.autoFixOnSave": true,
  "eslint.validate":[
      {
          "language":"javascript",
          "autoFix": true
      },
],

//formata o arquivo quando ele e salvo
"editor.formatOnSave": false,
"editor.tabSize": 2,
"editor.renderLineHighlight": "gutter",

"editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
}
}